package com.iteaj.iot.test.client.api;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ClientInitiativeSyncProtocol;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.test.TestProtocolType;

public class ApiStopTestProtocol extends ClientInitiativeSyncProtocol<ApiTestClientMessage> {

    @Override
    protected ApiTestClientMessage doBuildRequestMessage() {
        DefaultMessageHead messageHead = new DefaultMessageHead("Api:Stop:1234", protocolType());
        messageHead.setMessage((messageHead.getEquipCode() + "\n").getBytes());
        return new ApiTestClientMessage(messageHead);
    }

    @Override
    public void doBuildResponseMessage(ApiTestClientMessage responseMessage) {

    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.CIReq;
    }
}
