package com.iteaj.iot.test.taos;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.iteaj.iot.tools.annotation.IotTableId;
import com.iteaj.iot.taos.STable;
import com.iteaj.iot.tools.annotation.IotField;
import com.iteaj.iot.tools.annotation.IotTable;
import com.iteaj.iot.tools.annotation.IotTag;

import java.util.Date;

/**
 * 测试直接使用数据表
 */
@IotTable(value = "t_collect_entity")
@STable(table = "t_${sn}")
public class TaosBreakerDataTable {

    @IotTag
    private String sn;
    @IotField
    private double v; // 电压
    @IotField
    private double i; // 电流
    @IotField
    private double power1; // 有功功率
    @IotField
    private double power2; // 无功功率
    @IotField
    private Double py; // 功率因素

    @IotTableId
    private Date ts;

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public double getV() {
        return v;
    }

    public void setV(double v) {
        this.v = v;
    }

    public double getI() {
        return i;
    }

    public void setI(double i) {
        this.i = i;
    }

    public double getPower1() {
        return power1;
    }

    public void setPower1(double power1) {
        this.power1 = power1;
    }

    public double getPower2() {
        return power2;
    }

    public void setPower2(double power2) {
        this.power2 = power2;
    }

    public Double getPy() {
        return py;
    }

    public void setPy(Double py) {
        this.py = py;
    }

    public TaosBreakerDataTable touchTs() {
        this.ts = DateUtil.offset(this.ts, DateField.MILLISECOND, 68); return this;
    }
}
