package com.iteaj.iot.udp;

import com.iteaj.iot.message.DefaultMessageBody;

public class UdpMessageBody extends DefaultMessageBody {

    public UdpMessageBody(byte[] message) {
        super(message);
    }
}
