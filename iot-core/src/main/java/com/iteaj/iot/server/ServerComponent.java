package com.iteaj.iot.server;

import com.iteaj.iot.DeviceManager;
import com.iteaj.iot.FrameworkComponent;

public interface ServerComponent extends FrameworkComponent {

    DeviceManager getDeviceManager();
}
