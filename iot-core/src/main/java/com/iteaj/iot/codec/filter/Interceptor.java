package com.iteaj.iot.codec.filter;

import com.iteaj.iot.FrameworkComponent;

/**
 * 组件拦截器, 用于过滤连接的各个环节
 * @param <C>
 */
public interface Interceptor<C extends FrameworkComponent> {

}
