package com.iteaj.iot.boot.core;

import com.iteaj.iot.IotCoreProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "iot.core")
public class CoreProperties extends IotCoreProperties {

}
